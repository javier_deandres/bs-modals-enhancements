/**
 * Bootstrap modals enhancements
 *
 * @version 1.0.0
 * @author Javier de Andrés <javier.dag@gmail.com>
 * @license MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 *
 */
// Fullscreen
$(".modal-fullscreen").on('show.bs.modal', function () {
    setTimeout(function() {
        $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
    }, 0);
});
$(".modal-fullscreen").on('hidden.bs.modal', function () {
    $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
});

// Transparent
$(".modal-transparent").on('show.bs.modal', function () {
    setTimeout(function() {
        $(".modal-backdrop").addClass("modal-backdrop-transparent");
    }, 0);
});
$(".modal-transparent").on('hidden.bs.modal', function () {
    $(".modal-backdrop").addClass("modal-backdrop-transparent");
});